const host = "localhost"; //broker.emqx.io ==> cloud free
const port = "1883";
const connectUrl = `mqtt://${host}:${port}`;

const clientId = `mqtt_${Math.random().toString(16).slice(3)}`;

const options = {
  clientId,
  clean: true,
  connectionTimeout: 4000,
  username: "emqx",
  password: "public",
  reconnectPeriod: 1000,
};

module.exports = { options, connectUrl };
