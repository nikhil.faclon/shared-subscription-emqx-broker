const mqtt = require("mqtt");

const { options, connectUrl } = require("./config");

const client = mqtt.connect(connectUrl, options);

const topic = "temperature";

client.on("connect", () => {
  client.subscribe(topic, () => {
    console.log(`Subscribe to topic '${topic}'`);
  });
});

client.on("message", (topic, message) => {
  console.log("Received message:", topic, message.toString());
});
