const Influx = require("influx");

const influx = new Influx.InfluxDB({
  host: "localhost",
  port: 8086,
  database: "devices",
  schema: [
    {
      measurement: "data",
      fields: { value: Influx.FieldType.FLOAT },
      tags: ["ACTIVE", "CUR1", "CUR2", "CUR3"],
    },
  ],
});

// module.exports = { influx };
