const mqtt = require("mqtt");
const { options, connectUrl } = require("./config");

const client = mqtt.connect(connectUrl, options);
let i = 1;

client.on("connect", () => {
  setInterval(() => {
    let random = Math.random() * 50;
    console.log(random);
    if (random > 20) {
      client.publish("temperature", i + " " + random.toString());
      i++;
    }
  }, 1000);
});
